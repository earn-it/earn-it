import { createApp } from 'vue'
import App from './App.vue'
import router from './router'
import "bootstrap/dist/css/bootstrap.min.css"
import "bootstrap"
import "./assets/styles/css/main.css"
import "bootstrap-icons/font/bootstrap-icons.css";
import '@/assets/styles/scss/global.scss' // specify the path depending on your file structure

createApp(App).use(router).mount('#app')

// Import the functions you need from the SDKs you need
import { initializeApp } from "firebase/app";
import { getAnalytics } from "firebase/analytics";
import { getStorage, ref } from "firebase/storage";
import { getAuth, onAuthStateChanged } from "firebase/auth";

// Your web app's Firebase configuration
// For Firebase JS SDK v7.20.0 and later, measurementId is optional
const firebaseConfig = {
    apiKey: "AIzaSyBVBU2dpY4wSl6t94acgGYw2RNjVyB4h6g",
    authDomain: "earn-it-dp.firebaseapp.com",
    projectId: "earn-it-dp",
    storageBucket: "earn-it-dp.appspot.com",
    messagingSenderId: "1006930615556",
    appId: "1:1006930615556:web:043849a0ab7f710bc74451",
    measurementId: "G-ZQ4ZZK2G0Y"
};

// Initialize Firebase
const app = initializeApp(firebaseConfig);
// const analytics = getAnalytics(app);
// Initialize Cloud Storage and get a reference to the service
const storage = getStorage(app);
export { app };
const auth = getAuth();
onAuthStateChanged(auth, (user) => {
    if (user) {
        localStorage.setItem("loggedIn", "true");
    } else {
        // User is signed out
        localStorage.setItem("loggedIn", "false");
    }
});