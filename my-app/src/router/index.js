import {createRouter, createWebHistory} from 'vue-router'
import HomeView from '../views/HomeView.vue'
import LoginView from "@/views/LoginView";
import RegistrationView from "@/views/RegistrationView";
import adminView from "@/views/AdminView";
import StudentDashboard from "@/views/StudentDashboard";
import ProfilePage from "@/views/ProfilePage";
import CompanyPostVacancy from "@/views/CompanyPostVacancy";
import CompanyDashboard from "@/views/CompanyDashboard";
import {getAuth} from "firebase/auth";
import {loginBackend, logout} from "@/helpers/userLog";
import StudentVacancies from "@/views/StudentVacancies"


const routes = [
    {
        path: '/',
        name: 'home',
        component: HomeView,

    },
    {
        path: "/login",
        name: "login",
        component: LoginView,
        meta: {
            authRequired: false,
        }
    },
    {
        path: "/register",
        name: "register",
        component: RegistrationView,
        meta: {
            authRequired: false,
        }
    },
    {
        path: "/student/dashboard",
        name: "Student Dashboard",
        component: StudentDashboard,
        meta: {
            authRequired: true,
            studentRequired: true,
        }
    },
    {
        path: "/student/vacancies",
        name: "Student Vacancies",
        component: StudentVacancies,
        meta: {
            authRequired: true,
            studentRequired: false,
        }
    },
    {
        path: "/company/postVacancy",
        name: "Vacancy Post",
        component: CompanyPostVacancy,
        meta: {
            authRequired: true,
            companyRequired: true,
        }
    },
    {
        path: "/company/dashboard",
        name: "Company Dashboard",
        component: CompanyDashboard,
        meta: {
            authRequired: true,
            companyRequired: true,
        }
    },
    {
        path: "/profile",
        name: "Student Profile",
        component: ProfilePage,
        meta: {
            authRequired: true,
            completeProfile: false,
            studentRequired: false,
        },
    },
    {
        path: "/admin/dashboard",
        name: "admin",
        component: adminView,
        meta: {
            authRequired: true,
            adminRequired: true,
        }
    }

]

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes
})

//First it is established if a user is logged in or not
//Then it is checked if the requested page needs authentication
//If this is the case then the login flag is checked, if the user is logged in then there is a check for userStatus and userID
//If one of these is null, then these are requested from the backend
//Then the status is used to determine if the user is allowed to access the page
router.beforeEach(async (to, from) => {
    const auth = getAuth();
    let loggedIn = (localStorage.getItem("loggedIn") === "true");
    console.log("loggedIn: " + loggedIn)
    console.log("to: " + to.name)
    console.log("from: " + from.name)
    console.log("authRequired: " + to.meta.authRequired)
    if (to.meta.authRequired) {
        console.log("auth required");
        if (loggedIn) {
            if (localStorage.getItem("userStatus") === null || localStorage.getItem("userID") === null) {
                await loginBackend();
                if (localStorage.getItem("userStatus") === null || localStorage.getItem("userID") === null) {
                    //if it is still null, then something went wrong with the backend and logout user
                    await logout()
                    return;
                }
            }
            let userStatus = localStorage.getItem("userStatus");
            let userID = localStorage.getItem("userID");
            //checks page access followed by user status, if not allowed sent to home
            if (to.meta.studentRequired) {
                console.log("student required");
                if (userStatus === "student") {
                    return true;
                } else {
                    alert("You must be a student to access this area");
                    return {name: "home"};
                }
            } else if (to.meta.companyRequired) {
                console.log("company required");
                if (userStatus === "company") {
                    return true;
                } else {
                    alert("You must be a company to access this area");
                    return {name: "home"};
                }
            } else if (to.meta.adminRequired) {
                console.log("admin required");
                if (userStatus === "admin") {
                    return true;
                } else {
                    alert("You must be an admin to access this area");
                    return {name: "home"};
                }
            } else {
                return true;
            }
        } else {
            alert("You must be logged in to access this area");
            return {name: "login"};
        }
    } else if (to.meta.authRequired === false && loggedIn) {
        console.log("here 222" + loggedIn);
        // if user is logged in but authorization is not allowed (e.g. login or register page), redirect to home
        let userStatus = localStorage.getItem("userStatus");
        console.log(userStatus);
        if (userStatus === "student") {
            return {name: "Student Dashboard"};
        } else if (userStatus === "company") {
            return {name: "Company Dashboard"};
        } else if (userStatus === "admin") {
            return {name: "admin"};
        } else {
            return {name: "home"};
        }
    } else {
        //for pages that don't require auth but can be accessed when logged in
        console.log("auth not required");
        return true;
    }
})



export default router
