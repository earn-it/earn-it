import {getAuth, signOut} from "firebase/auth";
import axios from "axios";
import router from "@/router";

export async function loginBackend() {
    const auth = getAuth()
    const idToken = await auth.currentUser.getIdToken();
    localStorage.setItem('token',idToken);
    console.log("idToken")
    console.log(idToken)
    // TODO edit this to use the correct backend url
    axios.get("http://localhost:8080/api/login", {
        headers: {
            Authorization: `Bearer ${idToken}`
        }
    }).then((response) => {
        console.log(response);       
        let data = response.data.split(".");
        localStorage.setItem("userID", data[0]);
        switch (data[1]) {
            case "Student":
                console.log("student case");
                localStorage.setItem("userStatus", "student");
                localStorage.setItem("loggedIn", "true");
                router.push({name: "Student Dashboard"});
                break;
            case "Company":
                localStorage.setItem("userStatus", "company");
                localStorage.setItem("loggedIn", "true");
                router.push({name: "Company Dashboard"});
                break;
            case "Admin":
                localStorage.setItem("userStatus", "admin");
                localStorage.setItem("loggedIn", "true");
                router.push({name: "admin"});
                break;
            default:
                console.log(data);
                console.log("invalid user status from backend logging out user");
                logout()
                break;
        }
    }).catch((error) => {
        logout(); //logout user if the backend throws an error
        console.log(error)
    })
}

export async function logout() {
    localStorage.clear();
    console.log("user is being logged out")
    const auth = getAuth();
    await signOut(auth);

    await router.push("/");
}

export function convertToURL (blob) {
    return new Promise((resolve, reject) => {
      const reader = new FileReader()
      reader.onerror = reject
      reader.onload = () => {
        resolve(reader.result)
      }
      reader.readAsDataURL(blob)
    })
  }


